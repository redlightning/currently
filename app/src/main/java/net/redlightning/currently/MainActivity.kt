package net.redlightning.currently

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.preference.PreferenceManager
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), LocationListener {
    private val locationPermissionRequestCode = 1337
    var is24hour: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        PreferenceManager.setDefaultValues(this, R.xml.root_preferences, false)
        updateValues()

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        is24hour = sharedPref.getBoolean("twentyFourHour", true)
        val thread: Thread = object : Thread() {
            override fun run() {
                try {
                    while (!this.isInterrupted) {
                        sleep(200)
                        runOnUiThread {
                            val timeLabel = findViewById<TextView>(R.id.lbl_time)
                            var timeFormatStr = "hh:mm:ss a"
                            if(is24hour) timeFormatStr = "HH:mm:ss"
                            val timeFormat = SimpleDateFormat(timeFormatStr, Locale.US)
                            val currentCalendar = Calendar.getInstance()
                            val currentTime = timeFormat.format(currentCalendar.time)
                            timeLabel.text = currentTime
                        }
                    }
                } catch (e: InterruptedException) {
                    Log.d("Currently", "Time thread interrupted")
                }
            }
        }
        thread.start()

        val settingsButton = findViewById<ImageButton>(R.id.btn_settings)
        settingsButton.setOnClickListener {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }
    }

    private fun updateValues() {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        is24hour = sharedPref.getBoolean("twentyFourHour", true)
        val calendar = Calendar.getInstance()
        val dateFormat = SimpleDateFormat(sharedPref.getString("dateFormat", "MM-dd-yyyy"), Locale.US)
        val timeZoneFormat = SimpleDateFormat("zzzz", Locale.US)
        val date = dateFormat.format(calendar.time)
        val timeZone = timeZoneFormat.format(calendar.time)

        val dateLabel = findViewById<TextView>(R.id.lbl_date)
        dateLabel.text = date
        val timezoneLabel = findViewById<TextView>(R.id.lbl_timezone)
        timezoneLabel.text = timeZone

        getLocation()
    }

    private fun getLocation() {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                locationPermissionRequestCode
            )
        } else {
            val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0f, this)
        }
    }

    override fun onLocationChanged(location: Location) {
        val latitudeLabel = findViewById<TextView>(R.id.lbl_latitude)
        val longitudeLabel = findViewById<TextView>(R.id.lbl_longitude)
        var longitude = location.longitude
        var latitude = location.latitude
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        when(sharedPref.getString("locationFormat", "Seconds")) {
            "Seconds" -> {
                var dirLong = "W"
                var dirLat = "S"
                if (location.longitude > 0)  dirLong = "E" else longitude *= -1
                if (location.latitude > 0) dirLat = "N" else latitude *= -1
                val strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS)
                val strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS)
                longitudeLabel.text = String.format("Lon: %s %s", strLongitude, dirLong)
                latitudeLabel.text = String.format("Lat: %s %s", strLatitude, dirLat)
            }
            "Minutes" -> {
                longitudeLabel.text = String.format("Lon: %s", Location.convert(longitude, Location.FORMAT_MINUTES))
                latitudeLabel.text = String.format("Lat: %s", Location.convert(latitude, Location.FORMAT_MINUTES))
            }
            "Degrees" -> {
                longitudeLabel.text = String.format("Lon: %s", Location.convert(longitude, Location.FORMAT_DEGREES))
                latitudeLabel.text = String.format("Lat: %s", Location.convert(latitude, Location.FORMAT_DEGREES))
            }
            else -> {
                longitudeLabel.text = String.format("Lon: %s", longitude)
                latitudeLabel.text = String.format("Lat: %s", latitude)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            locationPermissionRequestCode -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    if ((ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED)
                    ) {
                        getLocation()
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        updateValues()
    }
}
